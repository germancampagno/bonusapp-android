package com.bonusapp2.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.bonusapp2.fragments.ProductsFragment;
import com.bonusapp2.fragments.PromotionFragment;
import com.bonusapp2.fragments.StoresFragment;

/**
 * Created by franco on 17/12/15.
 */
public class ViewPagerAdapter extends FragmentPagerAdapter {
    final int PAGE_COUNT = 3;
    Context context;
    //TAB TITLES
    private String tabtitles[] = new String[]{"Productos", "Promociones", "Tiendas"};

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                ProductsFragment frag1 = new ProductsFragment();
                return frag1;
            case 1:
                PromotionFragment frag2 = new PromotionFragment();
                return frag2;
            case 2:
                StoresFragment frag3 = new StoresFragment();
                return frag3;
        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabtitles[position];
    }
}