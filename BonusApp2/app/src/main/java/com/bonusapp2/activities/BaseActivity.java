package com.bonusapp2.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;

import com.bonusapp2.R;


/**
 * Created by German on 21/12/2015.
 */
public class BaseActivity extends FragmentActivity {

    private Context context;
    private boolean animBack = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = BaseActivity.this;
    }

//    protected void showError(HandlerResponseData.TypeError typeError, String message) {
//
//        if (typeError.equals(HandlerResponseData.TypeError.NO_INTERNET)) {
//            AlertClass.showAlert(getString(R.string.error_internet_connection), context);
//        } else if (typeError.equals(TypeError.NO_RESPONSE_WEB_SERVICE)) {
//            AlertClass.showAlert(getString(R.string.error_server), context);
//        } else if (typeError.equals(TypeError.NO_PERMISSION_FACEBOOK)) {
//            AlertClass.showAlert(getString(R.string.error_permission_facebook), context);
//        } else if (typeError.equals(TypeError.ERROR_FROM_SERVER)) {
//            AlertClass.showAlert(message, context);
//        }
//    }

    protected void showBackButtonActionBar() {
        findViewById(R.id.iv_show_side_bar).setVisibility(View.INVISIBLE);
        findViewById(R.id.iv_back_button).setVisibility(View.VISIBLE);
        findViewById(R.id.cont_menu_lateral).setVisibility(View.INVISIBLE);
        findViewById(R.id.cont_buy).setVisibility(View.GONE);
        animBack = true;
        findViewById(R.id.iv_back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.slide_in_left_fragment,
                        R.anim.slide_out_right_fragment);
            }
        });
    }


    @Override
    public void onBackPressed() {
        if (animBack) {
            finish();
            overridePendingTransition(R.anim.slide_in_left_fragment,
                    R.anim.slide_out_right_fragment);
        } else
            super.onBackPressed();
    }

    protected void showActionBar() {
        findViewById(R.id.iv_show_side_bar).setVisibility(View.VISIBLE);
        findViewById(R.id.iv_back_button).setVisibility(View.INVISIBLE);
        findViewById(R.id.cont_menu_lateral).setVisibility(View.VISIBLE);
        findViewById(R.id.cont_buy).setVisibility(View.VISIBLE);
        animBack = true;
    }
}
