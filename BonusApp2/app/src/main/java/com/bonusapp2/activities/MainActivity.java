package com.bonusapp2.activities;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import com.astuetz.PagerSlidingTabStrip;
import com.bonusapp2.R;
import  com.bonusapp2.entities.OptionData;
import  com.bonusapp2.items.ItemOptionSideBar;
import  com.bonusapp2.adapters.ViewPagerAdapter;
import com.utilities.adapter.AdapterGeneric;
import com.utilities.adapter.Item;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity {
    List<Item> itemsListOpSideBar;
    ListView lvLeftSideBar;
    ViewPagerAdapter mPagerAdapter;
    ViewPager pager;
    ImageView ivShowSideBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        showActionBar();
        fillListItemsSideBar();
        AdapterGeneric adapter = new AdapterGeneric(MainActivity.this, itemsListOpSideBar);
        initView();
        lvLeftSideBar.setAdapter(adapter);
        pager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager()));
        PagerSlidingTabStrip tab = (PagerSlidingTabStrip) findViewById(R.id.tabs_main);
        tab.setViewPager(pager);
        initializePaging();
//        tvShowSideBar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                findViewById(R.id.layout_left_side_bar);
//            }
//        });
    }

    private void initView() {
        pager = (ViewPager) findViewById(R.id.mainPager);
        lvLeftSideBar = (ListView) findViewById(R.id.lv_left_drawer);
        ivShowSideBar = (ImageView) findViewById(R.id.iv_show_side_bar);
    }

    ////////////////SIDEBAR///////////////
    public OptionData newItemSideBar(int drawable, String title) {
        OptionData itemSideBar = new OptionData();
        itemSideBar.setTexto(title);
        itemSideBar.setImagen(drawable);
        return itemSideBar;
    }

    public void fillListItemsSideBar() {
        itemsListOpSideBar = new ArrayList<>();
        OptionData dataSideBar;
        dataSideBar = newItemSideBar(R.drawable.sidemenu_points_icon, getString(R.string.side_bar_points));
        ItemOptionSideBar item = new ItemOptionSideBar();
        item.setData(dataSideBar);
        itemsListOpSideBar.add(item);
        dataSideBar = newItemSideBar(R.drawable.sidemenu_coupon_icon, getString(R.string.side_bar_cupons));
        item = new ItemOptionSideBar();
        item.setData(dataSideBar);
        itemsListOpSideBar.add(item);
        dataSideBar = newItemSideBar(R.drawable.sidemenu_promo_icon, getString(R.string.side_bar_promotions));
        item = new ItemOptionSideBar();
        item.setData(dataSideBar);
        itemsListOpSideBar.add(item);
        dataSideBar = newItemSideBar(R.drawable.sidemenu_lastmov_icon, getString(R.string.side_bar_last_moves));
        item = new ItemOptionSideBar();
        item.setData(dataSideBar);
        itemsListOpSideBar.add(item);
        dataSideBar = newItemSideBar(R.drawable.sidemenu_settings_icon, getString(R.string.side_bar_config));
        item = new ItemOptionSideBar();
        item.setData(dataSideBar);
        itemsListOpSideBar.add(item);
        dataSideBar = newItemSideBar(R.drawable.sidemenu_help_icon, getString(R.string.side_bar_help));
        item = new ItemOptionSideBar();
        item.setData(dataSideBar);
        itemsListOpSideBar.add(item);
        dataSideBar = newItemSideBar(R.drawable.sidemenu_terms_icon, getString(R.string.side_bar_terms_and_conditions));
        item = new ItemOptionSideBar();
        item.setData(dataSideBar);
        itemsListOpSideBar.add(item);
    }

    ////////////////VIEW PAGER///////////////
    //-------VIEW PAGER ADAPTER

    ///---------INICIALIZAR PAGE
    private void initializePaging() {

        if (mPagerAdapter == null) {
            mPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
            pager.setAdapter(mPagerAdapter);
        } else {
            mPagerAdapter.notifyDataSetChanged();
            pager.setVisibility(View.VISIBLE);
            pager.setAdapter(mPagerAdapter);
        }
        pager.setCurrentItem(1);
    }
}
