package com.bonusapp2.items;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bonusapp2.R;
import com.utilities.adapter.Item;

import  com.bonusapp2.entities.OptionData;

/**
 * Created by franco on 14/12/15.
 */
public class ItemOptionSideBar implements Item {
    ViewHolder viewHolder;
    OptionData option;


    @Override
    public void setData(Object data) {
        if (data instanceof OptionData)
            option = (OptionData) data;
    }

    @Override
    public Object getData() {
        return null;
    }

    @Override
    public View getView(View view, final Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        viewHolder = new ViewHolder();

        if (view == null || !(view.getTag() instanceof ViewHolder)) {
            view = inflater.inflate(R.layout.item_option_side_bar, null);
            viewHolder.tvTitle = (TextView) view.findViewById(R.id.tv_item_option_side_bar);
            viewHolder.iv_option_side_bar = (ImageView) view.findViewById(R.id.iv_item_option_side_bar);
            viewHolder.tvTitle.setText(option.getTexto());
            viewHolder.iv_option_side_bar.setImageDrawable(context.getResources().getDrawable(option.getImagen()));
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        return view;
    }
    private class ViewHolder {
        private TextView tvTitle;
        private ImageView iv_option_side_bar;
    }


}
