package com.bonusapp2.items;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bonusapp2.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.utilities.adapter.Item;

import com.bonusapp2.entities.CategoryProductData;

/**
 * Created by franco on 17/12/15.
 */
public class ItemCategoryProduct implements Item {
    ViewHolder viewHolder;
    CategoryProductData categoryProdu;

    @Override
    public void setData(Object data) {
        if (data instanceof CategoryProductData)
            categoryProdu = (CategoryProductData) data;
    }

    @Override
    public Object getData() {
        return null;
    }

    ImageLoader imageLoader = ImageLoader.getInstance();
    private DisplayImageOptions options;

    public void setImageLoaderOptions(DisplayImageOptions options) {
        this.options = options;
    }

        @Override
    public View getView(View view, final Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        viewHolder = new ViewHolder();

        if (view == null || !(view.getTag() instanceof ViewHolder)) {
            view = inflater.inflate(R.layout.item_category_product, null);
            viewHolder.tv_category_item = (TextView) view.findViewById(R.id.tv_category_item);
            viewHolder.iv_category_product_item = (ImageView) view.findViewById(R.id.iv_category_product_item);
                view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        viewHolder.tv_category_item.setText(categoryProdu.getTexto());
            viewHolder.iv_category_product_item.setImageResource(categoryProdu.getImagen());
//        imageLoader.displayImage(categoryProdu.getImagen(), viewHolder.iv_category_product_item, options);
        return view;
    }
    private class ViewHolder {
        private TextView tv_category_item;
        private ImageView iv_category_product_item;
    }




}