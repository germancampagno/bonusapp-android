package com.bonusapp2;

/**
 * Created by German on 21/12/2015.
 */
import android.app.Application;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class BonusApp extends Application {

    ImageLoader imageLoader;


    @Override
    public void onCreate() {
        super.onCreate();
        imageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(getApplicationContext()));

    }
}