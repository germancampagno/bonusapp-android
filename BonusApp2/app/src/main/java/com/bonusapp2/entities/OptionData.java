package com.bonusapp2.entities;

/**
 * Created by German on 21/12/2015.
 */
import android.graphics.Bitmap;

/**
 * Created by franco on 14/12/15.
 */
public class OptionData {
    private String texto;
    private int imagen;

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }
    public int getImagen() {
        return imagen;
    }

    public void setImagen(int imagen) {
        this.imagen = imagen;
    }
}