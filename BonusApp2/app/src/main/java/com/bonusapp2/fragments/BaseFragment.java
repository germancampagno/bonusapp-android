package com.bonusapp2.fragments;

/**
 * Created by German on 21/12/2015.
 */

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;

/**
 * Created by franco on 14/12/15.
 */
public class BaseFragment extends Fragment {
    private Context context;
    private boolean animBack = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
    }
//    protected void initViewPager(ViewPager mPager, SlidingTabLayout mSlidingTabLayout, FragmentPagerAdapter mPagerAdapter) {
//
//        mPager.setAdapter(mPagerAdapter);
//        // mSlidingTabLayout.setCustomTabView(R.layout.tab_indicator, android.R.id.text1);
//        mSlidingTabLayout.setSelectedIndicatorColors(getResources().getColor(R.color.transparent));
//        mSlidingTabLayout.setCustomColorAndStyle(getResources().getColor(R.color.black), getResources().getColor(R.color.black));
//        mSlidingTabLayout.setDistributeEvenly(true);
//        mSlidingTabLayout.setViewPager(mPager);
//    }


}
