package com.bonusapp2.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bonusapp2.R;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import  com.bonusapp2.activities.ProductDetailActivity;
import  com.bonusapp2.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.utilities.adapter.AdapterGeneric;
import com.utilities.adapter.Item;

import java.util.ArrayList;
import java.util.List;

import  com.bonusapp2.entities.CategoryProductData;
import  com.bonusapp2.items.ItemCategoryProduct;


public class ProductsFragment extends Fragment {
    private View view;
    GridView gvProductsCategories;
    List<Item> itemsGV;
    ImageLoader imageLoader;
    DisplayImageOptions options;

    public ProductsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_products, container, false);
        initView();
        initImageLoader();
        fillGridViewItems();
        AdapterGeneric adapter = new AdapterGeneric(getContext(), itemsGV);
        gvProductsCategories.setAdapter(adapter);
        gvProductsCategories.setClickable(true);
        gvProductsCategories.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
//                Object o = gvProductsCategories.getItemAtPosition(position);
                Intent intentShowProdDetail = new Intent(getContext(), ProductDetailActivity.class);
                startActivity(intentShowProdDetail);
            }
        });
        return view;
    }

    private void initImageLoader() {
        options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
//                .showImageForEmptyUri(R.drawable.default_avatar)
//                .showImageOnFail(R.drawable.default_avatar)
//                .showImageOnLoading(R.drawable.default_avatar)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .resetViewBeforeLoading(true)
                .considerExifParams(true).build();

        imageLoader = ImageLoader.getInstance();
    }
    public  void initView()
    {
        gvProductsCategories =(GridView) view.findViewById(R.id.gv_products_categories);
    }
    ////LLENAR ITEMS DEL DATAGRID CON CATERIESPRODUCTS
    public CategoryProductData newItemGV(int drawable, String title)
    {
        CategoryProductData itemGVProducts = new CategoryProductData();
        itemGVProducts.setTexto(title);
        itemGVProducts.setImagen(drawable);
        return itemGVProducts;
    }
    public void fillGridViewItems() {
        itemsGV = new ArrayList<>();
        CategoryProductData dataGVProductCategory;
        dataGVProductCategory = newItemGV(R.drawable.kids_image, getString(R.string.category_jovenes_y_niños));
        ItemCategoryProduct item = new ItemCategoryProduct();
        item.setData(dataGVProductCategory);
        itemsGV.add(0,item);
        dataGVProductCategory = newItemGV(R.drawable.women_image, getString(R.string.category_para_ella));
        item = new ItemCategoryProduct();
        item.setData(dataGVProductCategory);
        itemsGV.add(1,item);
        dataGVProductCategory = newItemGV(R.drawable.men_image, getString(R.string.category_para_el));
        item = new ItemCategoryProduct();
        item.setData(dataGVProductCategory);
        itemsGV.add(2,item);
        dataGVProductCategory = newItemGV(R.drawable.tech_image, getString(R.string.category_tech));
        item = new ItemCategoryProduct();
        item.setData(dataGVProductCategory);
        itemsGV.add(3,item);
        dataGVProductCategory = newItemGV(R.drawable.home_and_kitchen_image, getString(R.string.category_home_and_kitchen));
        item = new ItemCategoryProduct();
        item.setData(dataGVProductCategory);
        itemsGV.add(4,item);
        dataGVProductCategory = newItemGV(R.drawable.summer_image, getString(R.string.category_summer));
        item = new ItemCategoryProduct();
        item.setData(dataGVProductCategory);
        itemsGV.add(5,item);
        dataGVProductCategory = newItemGV(R.drawable.services_image, getString(R.string.category_services));
        item = new ItemCategoryProduct();
        item.setData(dataGVProductCategory);
        itemsGV.add(6,item);
        dataGVProductCategory = newItemGV(R.drawable.home_appliances, getString(R.string.category_home_appliances));
        item = new ItemCategoryProduct();
        item.setData(dataGVProductCategory);
        itemsGV.add(7,item);

        for(Item mItem : itemsGV)
        {
            ((ItemCategoryProduct) mItem).setImageLoaderOptions(options);
        }
    }
}
