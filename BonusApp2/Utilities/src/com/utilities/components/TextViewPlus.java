package com.utilities.components;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.widget.TextView;

import com.infinix.utilities.R;

public class TextViewPlus extends TextView {

	private static final String TAG = "TextViewPlus";
	private static Map<String, Typeface> typefaces = null;
	private Paint textPaint;
	private float preferredTextSize;
	private float minTextSize;
	private boolean adjustText;

	public TextViewPlus(Context context) {
		super(context);
	}

	public TextViewPlus(Context context, AttributeSet attrs) {
		// super(context, attrs);
		this(context, attrs, R.attr.autoScaleTextViewStyle);
		// setCustomFont(context, attrs);
	}

	/**
	 * xmlns:utility="http://schemas.android.com/apk/res/com.appstarters.fan10"
	 * xmlns:tools="http://schemas.android.com/tools"
	 * <com.utilities.components.TextViewPlus />
	 * 
	 * @param context
	 * @param attrs
	 * @param defStyle
	 */
	public TextViewPlus(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setCustomFont(context, attrs);

		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.TextViewPlus, defStyle, 0);
		if (a.getBoolean(R.styleable.TextViewPlus_adjustText, false)) {
			this.adjustText = a.getBoolean(R.styleable.TextViewPlus_adjustText,
					false);
			this.textPaint = new Paint();
			this.minTextSize = a.getDimension(
					R.styleable.TextViewPlus_minTextSize, 10f);
			a.recycle();
			this.preferredTextSize = this.getTextSize();
		}

	}

	private void setCustomFont(Context ctx, AttributeSet attrs) {
		if (typefaces == null)
			typefaces = new HashMap<String, Typeface>(3);

		TypedArray a = ctx.obtainStyledAttributes(attrs,
				R.styleable.TextViewPlus);
		String customFont = a.getString(R.styleable.TextViewPlus_customFont);
		setCustomFont(ctx, customFont);
		a.recycle();
	}

	public boolean setCustomFont(Context ctx, String asset) {
		Typeface tf = null;
		if (typefaces.containsKey(asset)) {
			tf = typefaces.get(asset);
		} else {
			try {
				tf = Typeface.createFromAsset(ctx.getAssets(), asset);
				typefaces.put(asset, tf);
			} catch (Exception e) {
				Log.e(TAG, "Could not get typeface: " + e.getMessage());
				return false;
			}
		}
		setTypeface(tf);
		return true;
	}

	/**
	 * Set the minimum text size for this view
	 * 
	 * @param minTextSize
	 *            The minimum text size
	 */
	public void setMinTextSize(float minTextSize) {
		this.minTextSize = minTextSize;
	}

	/**
	 * Resize the text so that it fits
	 * 
	 * @param text
	 *            The text. Neither <code>null</code> nor empty.
	 * @param textWidth
	 *            The width of the TextView. > 0
	 */
	private void refitText(String text, int textWidth) {
		if (!adjustText)
			return;

		if (textWidth <= 0 || text == null || text.length() == 0)
			return;

		// the width
		int targetWidth = textWidth - this.getPaddingLeft()
				- this.getPaddingRight();

		final float threshold = 0.5f; // How close we have to be

		this.textPaint.set(this.getPaint());

		while ((this.preferredTextSize - this.minTextSize) > threshold) {
			float size = (this.preferredTextSize + this.minTextSize) / 2;
			this.textPaint.setTextSize(size);
			if (this.textPaint.measureText(text) >= targetWidth)
				this.preferredTextSize = size; // too big
			else
				this.minTextSize = size; // too small
		}
		// Use min size so that we undershoot rather than overshoot
		this.setTextSize(TypedValue.COMPLEX_UNIT_PX, this.minTextSize);
	}

	@Override
	protected void onTextChanged(final CharSequence text, final int start,
			final int before, final int after) {
		if (!adjustText)
			return;
		this.refitText(text.toString(), this.getWidth());
	}

	@Override
	protected void onSizeChanged(int width, int height, int oldwidth,
			int oldheight) {
		if (!adjustText)
			return;
		if (width != oldwidth)
			this.refitText(this.getText().toString(), width);
	}

	@Override
	public void setText(CharSequence text, BufferType type) {
		// TODO Auto-generated method stub
		super.setText(text, type);
	}

}