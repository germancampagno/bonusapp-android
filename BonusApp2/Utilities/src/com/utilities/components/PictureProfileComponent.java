package com.utilities.components;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.infinix.utilities.R;

public class PictureProfileComponent extends RelativeLayout {

	private AttributeSet attrs;
	private Context context;
	private Drawable defaultBackground;
	private ImageView photoProfile, iconTakeimage;
	private static final String TAG = "PICTURE_PROFILE_COMPONENT";
	private String mCurrentPhotoPath = "";
	private String nameDir = "";
	private Context ctx;
	public static final int ACTION_TAKE_PHOTO = 1;
	public static final int ACTION_GALLERY_PHOTO = 2;
	public static final int ACTION_DELETE_PHOTO = 3;
	private Activity activity;
	private Fragment fragment;
	private boolean canDelete = false;
	private SharedPreferences sharedPreferences;
	private String title;

	private boolean neverDelete = false;// idica que la imagen nunka
	// se debe borrar por ende si es true no aparece la opcion de eliminarla en
	// el dialogo.

	private PhotoContactDialog contactDialog;

	private File myPictureFile = null;

	private static final String JPEG_FILE_PREFIX = "IMG_";
	private static final String JPEG_FILE_SUFFIX = ".jpg";

	public ImageView getPhotoProfile() {
		return photoProfile;
	}

	public void setPhotoProfile(ImageView photoProfile) {
		this.photoProfile = photoProfile;
	}

	public ImageView getIconTakeimage() {
		return iconTakeimage;
	}

	public void setIconTakeimage(ImageView iconTakeimage) {
		this.iconTakeimage = iconTakeimage;
	}

	public PictureProfileComponent(Context context) {
		super(context);
		ctx = context;
		init(context, null, R.layout.picture_profile_layout);
	}

	public PictureProfileComponent(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs, R.layout.picture_profile_layout);
	}

	public PictureProfileComponent(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs, R.layout.picture_profile_layout);
	}

	public void changeBackground() {
		setBackgroundColor(Color.TRANSPARENT);
	}

	public void setContentViewCustom(Context context, int layout) {
		init(context, null, layout);
	}

	private void init(Context context, AttributeSet attrs, int layout) {

		this.context = context;

		if (sharedPreferences == null)
			sharedPreferences = context.getSharedPreferences(TAG,
					Context.MODE_PRIVATE);

		nameDir = context.getString(R.string.app_name);
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(layout, this, true);
		view.setBackgroundColor(Color.TRANSPARENT);

		setBackgroundColor(Color.TRANSPARENT);
		photoProfile = (ImageView) view
				.findViewById(R.id.iv_photo_profile_component);
		iconTakeimage = (ImageView) view
				.findViewById(R.id.iv_photo_button_profile_component);

		if (attrs != null) {
			this.attrs = attrs;
			TypedArray a = context.obtainStyledAttributes(attrs,
					R.styleable.PictureProfileComponent, 0, 0);

			photoProfile.getLayoutParams().height = (int) a.getDimension(
					R.styleable.PictureProfileComponent_height_photo_profile,
					100);
			photoProfile.getLayoutParams().width = (int) a.getDimension(
					R.styleable.PictureProfileComponent_width_photo_profile,
					100);

			photoProfile
					.setImageResource((int) a
							.getResourceId(
									R.styleable.PictureProfileComponent_background_photo_profile,
									R.drawable.profile_photo));
			defaultBackground = photoProfile.getDrawable();

			iconTakeimage.getLayoutParams().height = (int) a.getDimension(
					R.styleable.PictureProfileComponent_height_button_profile,
					100);

			iconTakeimage.getLayoutParams().width = (int) a.getDimension(
					R.styleable.PictureProfileComponent_width_button_profile,
					100);

			iconTakeimage
					.setImageResource((int) a
							.getResourceId(
									R.styleable.PictureProfileComponent_background_button_profile,
									R.drawable.btn_camera));

			a.recycle();
		}

	}// init

	public void initActionPicture(Activity activity) {

		this.activity = activity;
		initActionPicture();
	}

	public void initActionPicture(Fragment fragment) {
		this.fragment = fragment;
		initActionPicture();
	}

	private void initActionPicture() {

		iconTakeimage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (fragment != null) {
					boolean canDeletePhoto;
					if (neverDelete)
						canDeletePhoto = false;
					else
						canDeletePhoto = isCanDelete();

					contactDialog = new PhotoContactDialog(fragment
							.getActivity(), canDeletePhoto);
				} else if (activity != null) {
					contactDialog = new PhotoContactDialog(activity,
							isCanDelete());
				} else {
					contactDialog = null;
					return;
				}

				contactDialog.getTitleTextView().setText(title);

				contactDialog.setOnGalleryListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						dispatchGalleryIntent();
						contactDialog.dismiss();
					}
				});
				contactDialog.setOnTakePictureListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						dispatchTakePictureIntent();
						contactDialog.dismiss();

					}
				});

				contactDialog.setOnRemoveListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						removePhoto();
						contactDialog.dismiss();

					}
				});
				contactDialog.show();

			}
		});// onclick
	}

	private void removePhoto() {

		String path = getPath();

		if (!path.equals("") && path.contains(getNameDir())) {
			myPictureFile = new File(path);
			myPictureFile.delete();
			myPictureFile = null;
		}

		if (defaultBackground != null)
			photoProfile.setImageDrawable(defaultBackground);
		else
			photoProfile.setImageResource(R.drawable.profile_photo);

		canDelete = false;
		saveCantDelete(canDelete);
		savepath("");

		if (activity != null) {
			// permite a visar al la clases que utiliza este componente que se
			// elimino la foto
			Intent i = new Intent(activity, DeleteItemActivity.class);
			try {
				activity.startActivityForResult(i, ACTION_DELETE_PHOTO);
			} catch (android.content.ActivityNotFoundException exeption) {
				Log.e(TAG + "-ELIMINAR FOTO:",
						"para usar esta funcionalidad usted deberia declarar: com.infinix.utilidades.DeleteItemActivity en el manifest de su proyecto ");
			}

		} else if (fragment != null) {
			Intent i = new Intent(fragment.getActivity(),
					DeleteItemActivity.class);

			try {
				fragment.getActivity().startActivityForResult(i,
						ACTION_DELETE_PHOTO);
			} catch (android.content.ActivityNotFoundException exeption) {
				Log.e(TAG + "-ELIMINAR FOTO:",
						"para usar esta funcionalidad usted deberia agregar: com.infinix.utilidades.DeleteItemActivity en el manifest de su proyecto ");
			}
		}
	}// remove photo

	private void dispatchGalleryIntent() {
		Intent i = new Intent(Intent.ACTION_PICK,
				android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
		if (activity != null)
			activity.startActivityForResult(i, ACTION_GALLERY_PHOTO);
		else if (fragment != null)
			fragment.getActivity().startActivityForResult(i,
					ACTION_GALLERY_PHOTO);
	}

	private void dispatchTakePictureIntent() {
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		try {
			myPictureFile = createImageFile();
			mCurrentPhotoPath = myPictureFile.getAbsolutePath();
			takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
					Uri.fromFile(myPictureFile));
		} catch (IOException e) {
			e.printStackTrace();
			myPictureFile = null;
			mCurrentPhotoPath = null;
		}
		if (activity != null)
			activity.startActivityForResult(takePictureIntent,
					ACTION_TAKE_PHOTO);
		else if (fragment != null)
			fragment.getActivity().startActivityForResult(takePictureIntent,
					ACTION_TAKE_PHOTO);
	}

	public void removeMyTakePhoto() {
		String path = getPath();

		if (!path.equals("") && path.contains(getNameDir())) {
			myPictureFile = new File(path);
			myPictureFile.delete();
			myPictureFile = null;
			savepath("");
		}

	}// removemytekepohoto

	private File createImageFile() throws IOException {
		// Create an image file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
				.format(new Date());
		String imageFileName = JPEG_FILE_PREFIX + timeStamp + JPEG_FILE_SUFFIX;
		String nameAlbum = Environment.getExternalStorageDirectory()
				.getAbsolutePath() + "/" + nameDir;// getString(R.string.app_name);
		File albumF = new File(nameAlbum);
		if (!albumF.exists()) {
			albumF.mkdirs();
			File noMedia = new File(albumF, ".nomedia");
			noMedia.createNewFile();
		}

		File imageF = new File(albumF, imageFileName);
		imageF.createNewFile();
		return imageF;
	}

	/**
	 * Load empty in ImageView
	 */

	public void initImage() {
		if (photoProfile != null) {
			photoProfile.setImageDrawable(null);
			photoProfile.refreshDrawableState();
			photoProfile.setBackgroundColor(Color.TRANSPARENT);

		}
	}

	public void savepath(String path) {
		Editor prefsEditor = sharedPreferences.edit();
		prefsEditor.putString(TAG + "-path:", path);
		prefsEditor.commit();
	}

	public String getPath() {
		return sharedPreferences.getString(TAG + "-path:", "");
	}

	public void saveCantDelete(boolean cantDelete) {
		Editor prefsEditor = sharedPreferences.edit();
		prefsEditor.putBoolean(TAG + "-canDelete:", cantDelete);
		prefsEditor.commit();
	}

	public boolean getCanDelete() {
		return sharedPreferences.getBoolean(TAG + "-canDelete:", false);
	}

	public String getmCurrentPhotoPath() {
		return mCurrentPhotoPath;
	}

	public String getNameDir() {
		return nameDir;
	}

	public void setNameDir(String nameDir) {
		this.nameDir = nameDir;
	}

	public boolean isCanDelete() {

		return getCanDelete();
	}

	public void setCanDelete(boolean canDelete) {
		saveCantDelete(canDelete);
	}

	public void setTitleTakeDialogoPhoto(String title) {
		this.title = title;
	}// setTitleTakeDialogoPhoto

	public boolean isNeverDelete() {
		return neverDelete;
	}

	public void setNeverDelete(boolean neverDelete) {
		this.neverDelete = neverDelete;
	}

}// class
