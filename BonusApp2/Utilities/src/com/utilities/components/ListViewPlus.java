package com.utilities.components;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;

import com.utilities.listener.OnDetectOverScrollListener;
import com.utilities.listener.OnDetectScrollListener;

public class ListViewPlus extends android.widget.ListView {

	private OnScrollListener onScrollListener;
	private OnDetectScrollListener onDetectScrollListener;
	private OnDetectOverScrollListener onDetectOverScrollListener;

	public ListViewPlus(Context context) {
		super(context);
		onCreate(context, null, null);
	}

	public ListViewPlus(Context context, AttributeSet attrs) {
		super(context, attrs);
		onCreate(context, attrs, null);
	}

	public ListViewPlus(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		onCreate(context, attrs, defStyle);
	}

	private void onCreate(Context context, AttributeSet attrs, Integer defStyle) {
		setListeners();
	}

	private int oldTop;
	private int oldFirstVisibleItem;

	private void setListeners() {
		super.setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				if (onScrollListener != null) {
					onScrollListener.onScrollStateChanged(view, scrollState);
				}
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				if (onScrollListener != null) {
					onScrollListener.onScroll(view, firstVisibleItem,
							visibleItemCount, totalItemCount);
				}

				if (onDetectScrollListener != null) {
					onDetectedListScroll(view, firstVisibleItem);
				}
			}

			private void onDetectedListScroll(AbsListView absListView,
					int firstVisibleItem) {
				View view = absListView.getChildAt(0);
				int top = (view == null) ? 0 : view.getTop();

				if (firstVisibleItem == oldFirstVisibleItem) {
					if (top > oldTop) {
						onDetectScrollListener.onUpScrolling();
					} else if (top < oldTop) {
						onDetectScrollListener.onDownScrolling();
					}
				} else {
					if (firstVisibleItem < oldFirstVisibleItem) {
						onDetectScrollListener.onUpScrolling();
					} else {
						onDetectScrollListener.onDownScrolling();
					}
				}

				oldTop = top;
				oldFirstVisibleItem = firstVisibleItem;
			}
		});
	}

	@Override
	protected void onOverScrolled(int scrollX, int scrollY, boolean clampedX,
			boolean clampedY) {
		if (clampedY) {

			if (oldFirstVisibleItem == 0) {
				Log.i(VIEW_LOG_TAG, "OverTopScroll");
				if (onDetectOverScrollListener != null) {
					onDetectOverScrollListener.onOverTopScrolling();
				}
			} else {
				if (onDetectOverScrollListener != null) {
					onDetectOverScrollListener.onOverBottomScrolling();
				}
			}
		}
		super.onOverScrolled(scrollX, scrollY, clampedX, clampedY);
	}

	@Override
	public void setOnScrollListener(OnScrollListener onScrollListener) {
		this.onScrollListener = onScrollListener;
	}

	public void setOnDetectScrollListener(
			OnDetectScrollListener onDetectScrollListener) {
		this.onDetectScrollListener = onDetectScrollListener;
	}

	public OnDetectOverScrollListener getOnDetectOverScrollListener() {
		return onDetectOverScrollListener;
	}

	public void setOnDetectOverScrollListener(
			OnDetectOverScrollListener onDetectOverScrollListener) {
		this.onDetectOverScrollListener = onDetectOverScrollListener;
	}
}