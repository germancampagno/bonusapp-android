package com.utilities.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

public class GenericUtilities {

	private static final String TAG = "GenericUtilities";

	public static String getDataDir(Context context) throws Exception {
		return context.getPackageManager().getPackageInfo(
				context.getPackageName(), 0).applicationInfo.dataDir;
	}

	public static String readFromfileAssets(String fileName, Context context) {

		StringBuilder readFile = new StringBuilder();

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					context.getAssets().open(fileName), "UTF-8"));

			// do reading, usually loop until end of file reading
			String mLine;

			while ((mLine = reader.readLine()) != null) {
				// process line
				readFile.append(mLine);

			}

			reader.close();
		} catch (IOException e) {
			// log the exception
			Log.i(TAG, "no se pudo abrir on leer el archivo" + fileName);
		}

		return readFile.toString();
	}

	/**
	 * Verifies if the entered email have a correct format.
	 * 
	 * @param email
	 *            entered email
	 * @return
	 * 
	 *         true - if the email is valid
	 * 
	 *         false - if the email is not valid
	 */
	public final static boolean isValidEmail(CharSequence email) {
		if (TextUtils.isEmpty(email)) {
			return false;
		} else {
			return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
		}
	}

	/**
	 * Clears all the layout's views.
	 * 
	 * @param view
	 *            activity's root view
	 */
	public final static void unbindDrawables(View view) {
		try {
			if (view.getBackground() != null)
				view.getBackground().setCallback(null);

			if (view instanceof ViewGroup) {
				for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
					unbindDrawables(((ViewGroup) view).getChildAt(i));
				}
				try {
					((ViewGroup) view).removeAllViews();
				} catch (UnsupportedOperationException ignore) {
					// if can't remove all view (e.g. adapter view) - no problem
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param activity
	 * @param packageName
	 * @return returns the application KeyHash
	 */
	public final static String getApplicationKeyHash(Activity activity,
			String packageName) {

		String keyHash = "";
		try {
			PackageInfo info = activity.getPackageManager().getPackageInfo(
					packageName, PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				keyHash = Base64.encodeToString(md.digest(), Base64.DEFAULT);
				Log.e("KeyHash:", keyHash);
			}
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		return keyHash;
	}

	public static boolean isTablet(Context context) {
		return (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;
	}

	/**
	 * Esconde el teclado en caso de que se este mostrando.
	 * 
	 * @param activity
	 *            Activity actual
	 */
	public static void hideKeyboard(Activity activity) {
		InputMethodManager inputManager = (InputMethodManager) activity
				.getSystemService(Context.INPUT_METHOD_SERVICE);

		// check if no view has focus:
		View v = activity.getCurrentFocus();
		if (v == null)
			return;

		inputManager.hideSoftInputFromWindow(v.getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);
	}
}
