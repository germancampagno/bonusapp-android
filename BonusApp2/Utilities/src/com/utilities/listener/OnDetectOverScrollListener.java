package com.utilities.listener;

public interface OnDetectOverScrollListener {

	void onOverTopScrolling();

	void onOverBottomScrolling();

}
