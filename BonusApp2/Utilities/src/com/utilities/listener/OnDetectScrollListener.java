package com.utilities.listener;

public interface OnDetectScrollListener {
	void onUpScrolling();

    void onDownScrolling();
}
