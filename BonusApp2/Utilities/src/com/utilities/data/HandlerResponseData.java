package com.utilities.data;

public interface HandlerResponseData {

	public enum TypeError {
		NO_INTERNET, NO_RESPONSE_WEB_SERVICE, ERROR_INPUT, NO_DATA, DATA_ALREADY_STORED, USER_WRONG, EMAIL_WRONG
	};

	public void onSuccessResponse(Object data);

	public void onErrorResponse(TypeError error);
}
