package com.utilities.data;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBAdapter extends SQLiteOpenHelper {
	private static String DB_PATH = "";
	public static String DB_PATH_TEMP = "";
	private static String DB_NAME = "";
	private static String DB_TEMP = "";
	private SQLiteDatabase myDataBase;
	private SQLiteDatabase myDataBaseTEMP;
	private final Context myContext;
	private static DBAdapter mDBConnection;

	/**
	 * Constructor
	 * 
	 * @param context
	 */

	@SuppressLint("SdCardPath")
	private DBAdapter(Context context) {
		super(context, DB_NAME, null, 1);
		this.myContext = context;

		DB_PATH = "/data/data/"
				+ context.getApplicationContext().getPackageName()
				+ "/databases/";
		DB_PATH_TEMP = "/data/data/"
				+ context.getApplicationContext().getPackageName()
				+ "/databases/";
		System.out.println("--------->DB_PATH" + DB_PATH);
	}

	public static void swapsDB() {

		Log.i("DBAdapter", "-- Swaping DBs --");
		synchronized (DBAdapter.class) {
			File dbTemp = new File(DB_PATH_TEMP + DB_TEMP);
			File db = new File(DB_PATH + DB_NAME);

			if (!db.delete())
				Log.e("DBAdapter", "ERROR deleting DB");

			if (!dbTemp.renameTo(db))
				Log.e("DBAdapter", "ERROR renaming DBTemp");

			// if (!dbTemp.delete())
			// Log.e("DBAdapter", "ERROR deleting DBTemp");
		}
	}

	/**
	 * getting Instance
	 * 
	 * @param context
	 * @return DBAdapter
	 */
	public static synchronized DBAdapter getDBAdapterInstance(Context context,
			String nameDataBase) {
		DB_NAME = nameDataBase;
		if (mDBConnection == null) {
			synchronized (DBAdapter.class) {
				if (mDBConnection == null)
					mDBConnection = new DBAdapter(context);
			}

		}

		return mDBConnection;
	}

	/**
	 * DBAdapter with handler temp database
	 * 
	 * @param context
	 * @param nameDataBase
	 * @param nameDatabaseTemp
	 * @return
	 */
	public static synchronized DBAdapter getDBAdapterInstance(Context context,
			String nameDataBase, String nameDatabaseTemp) {
		DB_NAME = nameDataBase;
		DB_TEMP = nameDatabaseTemp;
		if (mDBConnection == null) {
			synchronized (DBAdapter.class) {
				if (mDBConnection == null)
					mDBConnection = new DBAdapter(context);
			}

		}

		return mDBConnection;
	}

	// /**
	// * getting BDTemp Instance
	// *
	// * @param context
	// * @return DBAdapter
	// */
	// public static synchronized DBAdapter getDBAdapterInstanceTemp(Context
	// context, String nameDataBase) {
	// DB_TEMP = nameDataBase;
	// if (mDBConnectionTemp == null) {
	// synchronized (DBAdapter.class) {
	// if (mDBConnectionTemp == null)
	// mDBConnectionTemp = new DBAdapter(context, true);
	// }
	//
	// }
	//
	// return mDBConnectionTemp;
	// }

	// Creates an empty database on the system and rewrites it with your own
	// database.
	public void createDataBase() throws IOException {
		createDataBase(false);
	}

	public void createDataBaseTemp() throws IOException {
		createDataBase(true);
	}

	public void createDataBase(boolean isTemp) throws IOException {

		boolean dbExist;
		if (!isTemp) {
			dbExist = myContext.getDatabasePath(DB_NAME).exists(); // checkDataBase();
		} else
			dbExist = myContext.getDatabasePath(DB_TEMP).exists(); // checkDataBaseTemp();

		if (dbExist) {
			// do nothing - database already exist
		} else {
			// By calling following method
			// 1) an empty database will be created into the default system path
			// of your application
			// 2) than we overwrite that database with our database.
			this.getReadableDatabase();
			try {
				if (!isTemp)
					copyDataBase();
				else
					copyDataBaseTemp();

			} catch (IOException e) {
				throw new Error("Error COPIANDO LA BASE DE DATOS");
			}
		}
	}

	/**
	 * Check if the database already exist to avoid re-copying the file each
	 * time you open the application.
	 * 
	 * @return true if it exists, false if it doesn't
	 */
	public boolean checkDataBase() {
		return checkDataBase(false);
	}

	public boolean checkDataBaseTemp() {
		return checkDataBase(true);
	}

	public boolean checkDataBase(boolean isTemp) {

		SQLiteDatabase checkDB = null;
		try {

			String myPath;
			if (!isTemp) {
				myPath = DB_PATH + DB_NAME;
			} else
				myPath = DB_PATH_TEMP + DB_TEMP;

			checkDB = SQLiteDatabase.openDatabase(myPath, null,
					SQLiteDatabase.OPEN_READONLY);

		} catch (SQLiteException e) {
			// database does't exist yet.
		}
		if (checkDB != null) {

			checkDB.close();
		}
		return checkDB != null ? true : false;
	}

	/**
	 * Copies your database from your local assets-folder to the just created
	 * empty database in the system folder, from where it can be accessed and
	 * handled. This is done by transfering bytestream.
	 * */
	private void copyDataBase() throws IOException {
		copyDataBase(false);
	}

	private void copyDataBaseTemp() throws IOException {
		copyDataBase(true);
	}

	private void copyDataBase(boolean isTemp) throws IOException {

		// Open your local db as the input stream
		InputStream myInput;

		myInput = myContext.getAssets().open(DB_NAME);

		// Path to the just created empty db
		String outFileName;
		if (!isTemp) {
			outFileName = DB_PATH + DB_NAME;
		} else
			outFileName = DB_PATH_TEMP + DB_TEMP;

		// Open the empty db as the output stream
		OutputStream myOutput = new FileOutputStream(outFileName);
		// transfer bytes from the inputfile to the outputfile
		byte[] buffer = new byte[1024];
		int length;
		while ((length = myInput.read(buffer)) > 0) {
			myOutput.write(buffer, 0, length);
		}
		// Close the streams
		myOutput.flush();
		myOutput.close();
		myInput.close();
	}

	/**
	 * Open the database
	 * 
	 * @throws SQLException
	 */
	public void openDataBase() throws SQLException {
		openDataBase(false);
	}

	public void openDataBaseTemp() throws SQLException {
		openDataBase(true);
	}

	public void openDataBase(boolean isTemp) throws SQLException {
		String myPath;
		if (!isTemp) {
			myPath = DB_PATH + DB_NAME;
			myDataBase = SQLiteDatabase.openDatabase(myPath, null,
					SQLiteDatabase.OPEN_READWRITE);
		}

		else {
			myPath = DB_PATH_TEMP + DB_TEMP;
			myDataBaseTEMP = SQLiteDatabase.openDatabase(myPath, null,
					SQLiteDatabase.OPEN_READWRITE);
		}
	}

	/**
	 * Close the database if exist
	 */
	public synchronized void close() {
		close(false);
	}

	public synchronized void closeTemp() {
		close(true);
	}

	public synchronized void close(boolean isTemp) {
		SQLiteDatabase bd;
		if (!isTemp)
			bd = myDataBase;
		else
			bd = myDataBaseTEMP;

		if (bd != null)
			bd.close();

		super.close();
	}

	/**
	 * Call on creating data base for example for creating tables at run time
	 */

	public void onCreate(SQLiteDatabase db) {
	}

	/**
	 * can used for drop tables then call onCreate(db) function to create tables
	 * again - upgrade
	 */

	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	}

	// ----------------------- CRUD Functions ------------------------------

	/**
	 * This function used to select the records from DB.
	 * 
	 * String sql = "SELECT id FROM " + TABLE;
	 * ArrayList<ArrayList<String>> result; synchronized (dataBase) {
	 * dataBase.openDataBase(); result = dataBase.selectRecordsFromDBList(sql,
	 * null); } dataBase.close();
	 * 
	 * 
	 * @param tableName
	 * @param tableColumns
	 * @param whereClase
	 * @param whereArgs
	 * @param groupBy
	 * @param having
	 * @param orderBy
	 * @return A Cursor object, which is positioned before the first entry.
	 */
	public Cursor selectRecordsFromDB(String tableName, String[] tableColumns,
			String whereClase, String whereArgs[], String groupBy,
			String having, String orderBy) {
		return selectRecordsFromDB(tableName, tableColumns, whereClase,
				whereArgs, groupBy, having, orderBy, false);
	}

	public Cursor selectRecordsFromDBTemp(String tableName,
			String[] tableColumns, String whereClase, String whereArgs[],
			String groupBy, String having, String orderBy) {
		return selectRecordsFromDB(tableName, tableColumns, whereClase,
				whereArgs, groupBy, having, orderBy, true);
	}

	public Cursor selectRecordsFromDB(String tableName, String[] tableColumns,
			String whereClase, String whereArgs[], String groupBy,
			String having, String orderBy, boolean isTemp) {

		SQLiteDatabase bd;
		if (!isTemp)
			bd = myDataBase;
		else
			bd = myDataBaseTEMP;

		return bd.query(tableName, tableColumns, whereClase, whereArgs,
				groupBy, having, orderBy);
	}

	/**
	 * select records from db and return in list
	 * 
	 * @param tableName
	 * @param tableColumns
	 * @param whereClase
	 * @param whereArgs
	 * @param groupBy
	 * @param having
	 * @param orderBy
	 * @return ArrayList<ArrayList<String>>
	 */
	public ArrayList<ArrayList<String>> selectRecordsFromDBList(
			String tableName, String[] tableColumns, String whereClase,
			String whereArgs[], String groupBy, String having, String orderBy) {
		return selectRecordsFromDBList(tableName, tableColumns, whereClase,
				whereArgs, groupBy, having, orderBy, false);
	}

	public ArrayList<ArrayList<String>> selectRecordsFromDBListTemp(
			String tableName, String[] tableColumns, String whereClase,
			String whereArgs[], String groupBy, String having, String orderBy) {
		return selectRecordsFromDBList(tableName, tableColumns, whereClase,
				whereArgs, groupBy, having, orderBy, true);
	}

	public ArrayList<ArrayList<String>> selectRecordsFromDBList(
			String tableName, String[] tableColumns, String whereClase,
			String whereArgs[], String groupBy, String having, String orderBy,
			boolean isTemp) {

		ArrayList<ArrayList<String>> retList = new ArrayList<ArrayList<String>>();
		ArrayList<String> list = new ArrayList<String>();

		SQLiteDatabase bd;
		if (!isTemp)
			bd = myDataBase;
		else
			bd = myDataBaseTEMP;

		Cursor cursor = bd.query(tableName, tableColumns, whereClase,
				whereArgs, groupBy, having, orderBy);
		if (cursor.moveToFirst()) {
			do {
				list = new ArrayList<String>();
				for (int i = 0; i < cursor.getColumnCount(); i++) {
					list.add(cursor.getString(i));
				}
				retList.add(list);
			} while (cursor.moveToNext());
		}
		if (cursor != null && !cursor.isClosed()) {
			cursor.close();
		}
		return retList;

	}

	public long insertRecordsInDB(String tableName, String nullColumnHack,
			ContentValues initialValues) {
		return insertRecordsInDB(tableName, nullColumnHack, initialValues,
				false);
	}

	public long insertRecordsInDBTemp(String tableName, String nullColumnHack,
			ContentValues initialValues) {
		return insertRecordsInDB(tableName, nullColumnHack, initialValues, true);
	}

	public long insertRecordsInDB(String tableName, String nullColumnHack,
			ArrayList<ContentValues> initialValues) {
		return insertRecordsInDB(tableName, nullColumnHack, initialValues,
				false);
	}

	public long insertRecordsInDBTemp(String tableName, String nullColumnHack,
			ArrayList<ContentValues> initialValues) {
		return insertRecordsInDB(tableName, nullColumnHack, initialValues, true);
	}

	/**
	 * 
	 * This function used to insert the Record in DB.
	 * 
	 * @param tableName
	 * @param nullColumnHack
	 * @param initialValues
	 * @return the row ID of the newly inserted row, or -1 if an error occurred
	 */
	private long insertRecordsInDB(String tableName, String nullColumnHack,
			ContentValues initialValues, boolean isTemp) {
		SQLiteDatabase bd;
		if (!isTemp)
			bd = myDataBase;
		else
			bd = myDataBaseTEMP;
		long lng = 0;

		lng = bd.insert(tableName, nullColumnHack, initialValues);
		bd.close();
		return lng;
	}

	private long insertRecordsInDB(String tableName, String nullColumnHack,
			ArrayList<ContentValues> initialValues, boolean isTemp) {
		SQLiteDatabase db;
		if (!isTemp)
			db = myDataBase;
		else
			db = myDataBaseTEMP;
		long lng = 0;

		db.beginTransaction();
		for (ContentValues entry : initialValues) {
			db.insert(tableName, nullColumnHack, entry);
		}
		db.setTransactionSuccessful();
		db.endTransaction();
		db.close();

		return lng;
	}

	public void insert_or_update(String sql) {
		insert_or_update(sql, false);
	}

	public void insert_or_updateTemp(String sql) {
		insert_or_update(sql, true);
	}

	public void insert_or_update(String sql, boolean isTemp) {

		SQLiteDatabase bd;
		if (!isTemp)
			bd = myDataBase;
		else
			bd = myDataBaseTEMP;

		synchronized (bd) {
			// bd.beginTransaction();
			bd.execSQL(sql);
			// bd.setTransactionSuccessful();
			// bd.endTransaction();
		}

	}

	/**
	 * This function used to update the Record in DB.
	 * 
	 * @param tableName
	 * @param initialValues
	 * @param whereClause
	 * @param whereArgs
	 * @return 0 in case of failure otherwise return no of row(s) are updated
	 */
	public int updateRecordsInDB(String tableName, ContentValues initialValues,
			String whereClause, String whereArgs[]) {
		return updateRecordsInDB(tableName, initialValues, whereClause,
				whereArgs, false);
	}

	public int updateRecordsInDBTemp(String tableName,
			ContentValues initialValues, String whereClause, String whereArgs[]) {
		return updateRecordsInDB(tableName, initialValues, whereClause,
				whereArgs, true);
	}

	private int updateRecordsInDB(String tableName,
			ContentValues initialValues, String whereClause,
			String whereArgs[], boolean isTemp) {
		SQLiteDatabase bd;
		if (!isTemp)
			bd = myDataBase;
		else
			bd = myDataBaseTEMP;
		int result = 0;
		// synchronized (bd) {
		// bd.beginTransaction();
		result = bd.update(tableName, initialValues, whereClause, whereArgs);
		// bd.setTransactionSuccessful();
		// bd.endTransaction();

		// }
		bd.close();
		return result;
	}

	/**
	 * This function used to delete the Record in DB.
	 * 
	 * dataBase.openDataBase(); dataBase.deleteRecordInDB(TABLE_MY_TEAMS, null,
	 * null); dataBase.close();
	 * 
	 * 
	 * @param tableName
	 * @param whereClause
	 * @param whereArgs
	 * @return 0 in case of failure otherwise return no of row(s) are deleted.
	 */
	public int deleteRecordInDB(String tableName, String whereClause,
			String[] whereArgs) {
		return deleteRecordInDB(tableName, whereClause, whereArgs, false);
	}

	public int deleteRecordInDBTemp(String tableName, String whereClause,
			String[] whereArgs) {
		return deleteRecordInDB(tableName, whereClause, whereArgs, true);
	}

	private int deleteRecordInDB(String tableName, String whereClause,
			String[] whereArgs, boolean isTemp) {

		SQLiteDatabase bd;
		if (!isTemp)
			bd = myDataBase;
		else
			bd = myDataBaseTEMP;

		int it = 0;
		synchronized (bd) {
			// bd.beginTransaction();
			it = bd.delete(tableName, whereClause, whereArgs);
			// bd.setTransactionSuccessful();
			// bd.endTransaction();

		}
		bd.close();
		return it;
	}

	// --------------------- Select Raw Query Functions ---------------------

	/**
	 * apply raw Query
	 * 
	 * @param query
	 * @param selectionArgs
	 * @return Cursor
	 */
	public Cursor selectRecordsFromDB(String query, String[] selectionArgs) {
		return selectRecordsFromDB(query, selectionArgs, false);
	}

	public Cursor selectRecordsFromDBTemp(String query, String[] selectionArgs) {
		return selectRecordsFromDB(query, selectionArgs, true);
	}

	public Cursor selectRecordsFromDB(String query, String[] selectionArgs,
			boolean isTemp) {

		SQLiteDatabase bd;
		if (!isTemp)
			bd = myDataBase;
		else
			bd = myDataBaseTEMP;
		Cursor cursor = null;

		bd.beginTransaction();
		cursor = bd.rawQuery(query, selectionArgs);
		bd.setTransactionSuccessful();
		bd.endTransaction();

		// synchronized (bd) {
		// cursor = bd.rawQuery(query, selectionArgs);
		// }
		if (cursor != null && !cursor.isClosed())
			cursor.close();

		bd.close();
		return cursor;
	}

	/**
	 * apply raw query and return result in list
	 * 
	 * @param query
	 * @param selectionArgs
	 * @return ArrayList<ArrayList<String>>
	 */
	public ArrayList<ArrayList<String>> selectRecordsFromDBList(String query,
			String[] selectionArgs) {
		return selectRecordsFromDBList(query, selectionArgs, false);
	}

	public ArrayList<ArrayList<String>> selectRecordsFromDBListTemp(
			String query, String[] selectionArgs) {
		return selectRecordsFromDBList(query, selectionArgs, true);
	}

	public ArrayList<ArrayList<String>> selectRecordsFromDBList(String query,
			String[] selectionArgs, boolean isTemp) {
		ArrayList<ArrayList<String>> retList = new ArrayList<ArrayList<String>>();
		ArrayList<String> list = new ArrayList<String>();

		SQLiteDatabase bd;
		if (!isTemp) {
			bd = myDataBase;
			if (bd == null || !bd.isOpen()) {
				openDataBase(isTemp);
				bd = myDataBase;
			}

		} else {
			bd = myDataBaseTEMP;
			if (bd == null || !bd.isOpen()) {
				openDataBase(isTemp);
				bd = myDataBaseTEMP;
			}

		}

		Cursor cursor = null;
		cursor = bd.rawQuery(query, selectionArgs);

		if (cursor.moveToFirst()) {
			do {
				list = new ArrayList<String>();
				for (int i = 0; i < cursor.getColumnCount(); i++) {
					list.add(cursor.getString(i));

				}
				retList.add(list);
			} while (cursor.moveToNext());
		}
		if (cursor != null && !cursor.isClosed()) {
			cursor.close();
		}

		bd.close();
		return retList;
	}

	public void createIfNeccesary() {
		createIfNeccesary(false);
	}

	public void createIfNeccesaryTemp() {
		createIfNeccesary(true);
	}

	public void createIfNeccesary(boolean isTemp) {
		boolean exist;
		if (!isTemp)
			exist = checkDataBase();
		else
			exist = checkDataBaseTemp();

		if (!exist) {
			try {
				if (!isTemp) {
					synchronized (mDBConnection) {
						createDataBase();
					}

				} else {
					synchronized (mDBConnection) {
						createDataBaseTemp();
					}
				}

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
