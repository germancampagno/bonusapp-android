package com.utilities.tabHost;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;

import com.infinix.utilities.R;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;

public class TabHostBase extends FragmentActivity {

	private TabHost tabHost;
	private HashMap<String, Stack<Fragment>> stack;
	private String currentTab;
	private String tabIdentifier;
	private Fragment baseFrament;
	private int idButtonXml;
	private int cont = 0;

	private ItemTabHost itemTabHost;
	private List<ItemTabHost> tabHostList;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tab_host_base);
		stack = new HashMap<String, Stack<Fragment>>();
		tabHostList = new ArrayList<ItemTabHost>();
		tabHost = (TabHost) findViewById(android.R.id.tabhost);
	}

	public void addFragmentToStack(String tabIdentifier, Fragment baseFragment,
			int idButtonXml) {

		this.baseFrament = new Fragment();
		this.tabIdentifier = new String();

		this.tabIdentifier = tabIdentifier;
		this.baseFrament = baseFragment;
		this.idButtonXml = idButtonXml;

		itemTabHost = new ItemTabHost(this.tabIdentifier, this.baseFrament,
				this.idButtonXml);
		tabHostList.add(itemTabHost);

		stack.put(this.tabIdentifier, new Stack<Fragment>());
		tabHost.setOnTabChangedListener(listener);
		tabHost.setup();
		initializeTabs();
	}


	TabHost.OnTabChangeListener listener = new TabHost.OnTabChangeListener() {
		public void onTabChanged(String tabId) {
			/* Set current tab.. */
			currentTab = tabId;

			if (stack.get(tabId).size() == 0) {
				/*
				 * First time this tab is selected. So add first fragment of
				 * that tab. Dont need animation, so that argument is false. We
				 * are adding a new fragment which is not present in stack. So
				 * add to stack is true.
				 */
				for (int i = 0; i < tabHostList.size(); i++) {
					if (tabId.equals(tabHostList.get(i).getTabIdentifier())) {
						pushFragments(tabId, tabHostList.get(i)
								.getBaseFrament(), false, true);
						break;
					}
				}
			} else {
				/*
				 * We are switching tabs, and target tab is already has atleast
				 * one fragment. No need of animation, no need of stack pushing.
				 * Just show the target fragment
				 */
				pushFragments(tabId, stack.get(tabId).lastElement(), false,
						false);
			}
		}
	};

	public void pushFragments(String tag, Fragment fragment,
			boolean shouldAnimate, boolean shouldAdd) {
		if (shouldAdd)
			stack.get(tag).push(fragment);
		FragmentManager manager = getSupportFragmentManager();
		FragmentTransaction ft = manager.beginTransaction();
		if (shouldAnimate)
			ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
		ft.replace(R.id.realtabcontent, fragment);
		ft.commit();
	}

	public void popFragments() {
		/*
		 * Select the second last fragment in current tab's stack.. which will
		 * be shown after the fragment transaction given below
		 */
		Fragment fragment = stack.get(currentTab).elementAt(
				stack.get(currentTab).size() - 2);

		/* pop current fragment from stack.. */
		stack.get(currentTab).pop();

		/*
		 * We have the target fragment in hand.. Just show it.. Show a standard
		 * navigation animation
		 */
		FragmentManager manager = getSupportFragmentManager();
		FragmentTransaction ft = manager.beginTransaction();
		ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
		ft.replace(R.id.realtabcontent, fragment);
		ft.commit();
	}

	@Override
	public void onBackPressed() {
		if (((BaseFragment) stack.get(currentTab).lastElement())
				.onBackPressed() == false) {
			/*
			 * top fragment in current tab doesn't handles back press, we can do
			 * our thing, which is
			 * 
			 * if current tab has only one fragment in stack, ie first fragment
			 * is showing for this tab. finish the activity else pop to previous
			 * fragment in stack for the same tab
			 */
			if (stack.get(currentTab).size() == 1) {
				super.onBackPressed(); // or call finish..
			} else {
				popFragments();
			}
		} else {
			// do nothing.. fragment already handled back button press.
		}
	}

	/*
	 * Imagine if you wanted to get an image selected using ImagePicker intent
	 * to the fragment. Ofcourse I could have created a public function in that
	 * fragment, and called it from the activity. But couldn't resist myself.
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (stack.get(currentTab).size() == 0) {
			return;
		}

		/* Now current fragment on screen gets onActivityResult callback.. */
		stack.get(currentTab).lastElement()
				.onActivityResult(requestCode, resultCode, data);
	}

	/**
	 * Iniciliza botones del tab
	 */

	public void initializeTabs() {
		TabHost.TabSpec spec = tabHost.newTabSpec(tabHostList.get(cont)
				.getTabIdentifier());
		spec.setContent(new TabHost.TabContentFactory() {
			public View createTabContent(String tag) {
				return findViewById(R.id.realtabcontent);
			}
		});
		spec.setIndicator(createTabView(tabHostList.get(cont).getIdButtonXml()));
		tabHost.addTab(spec);
		cont++;

	}

	private View createTabView(final int id) {
		View view = LayoutInflater.from(this).inflate(R.layout.tabs_icon, null);
		ImageView imageView = (ImageView) view.findViewById(R.id.tab_icon);
		imageView.setImageDrawable(getResources().getDrawable(id));
		return view;
	}
}