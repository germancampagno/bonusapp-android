package com.utilities.tabHost;

import android.support.v4.app.Fragment;

public class ItemTabHost {

	private String tabIdentifier;
	private Fragment baseFrament;
	private int idButtonXml;
	
	
	public ItemTabHost(String tabIdentifier, Fragment baseFrament,
			int idButtonXml) {
		this.tabIdentifier = tabIdentifier;
		this.baseFrament= baseFrament;
		this.idButtonXml= idButtonXml;
	}
	public String getTabIdentifier() {
		return tabIdentifier;
	}
	public void setTabIdentifier(String tabIdentifier) {
		this.tabIdentifier = tabIdentifier;
	}
	public Fragment getBaseFrament() {
		return baseFrament;
	}
	public void setBaseFrament(Fragment baseFrament) {
		this.baseFrament = baseFrament;
	}
	public int getIdButtonXml() {
		return idButtonXml;
	}
	public void setIdButtonXml(int idButtonXml) {
		this.idButtonXml = idButtonXml;
	}
	
	
}
