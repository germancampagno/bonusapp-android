package com.utilities.adapter;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public class AdapterGeneric extends BaseAdapter {

	protected Context context;
	protected List<Item> itemsList;

	public AdapterGeneric(Context context, List<Item> itemsList) {
		this.context = context;
		this.itemsList = itemsList;
	}

	public int getCount() {
		return itemsList != null ? itemsList.size() : 0;
	}

	public Object getItem(int position) {
		return itemsList != null ? itemsList.get(position).getData() : null;
	}

	public long getItemId(int position) {
		return 0;
	}

	public View getView(int position, View convertView, ViewGroup parent) {

		return itemsList != null ? itemsList.get(position).getView(convertView,
				context) : null;
	}

	public void setItemsList(List<Item> itemsList) {
		this.itemsList = itemsList;
	}

}
